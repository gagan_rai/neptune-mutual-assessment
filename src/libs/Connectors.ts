import { InjectedConnector } from "@web3-react/injected-connector";
import { SUPPORTED_CHAINS } from '@/constants'


export const defaultConnector = new InjectedConnector({ supportedChainIds: SUPPORTED_CHAINS }) 