import { Token, TokenAmount } from '@uniswap/sdk'

export function formatAccount(account: string) {
  return `${account.substring(0, 6)}...${account.slice(-4)}`
}

export function formatAmount(token: Token, balance: string) {
  const tokenAmount = new TokenAmount(token, balance)
  return tokenAmount.toExact()
}
