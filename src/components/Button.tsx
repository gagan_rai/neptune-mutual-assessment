import classNames from "classnames";
import React from "react";

type ButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  scheme?: "primary" | "secondary" | "danger" | "outline";
};

const ButtonStyles = {
  primary: "bg-blue-600 hover:bg-blue-500",
  danger: "bg-red-600 hover:bg-red-500",
  secondary: "bg-slate-700 bg-opacity-20 hover:bg-opacity-100",
  outline: "bg-opacity-0 outline outline-1 hover:bg-white hover:text-black",
};

const Button = ({ children, scheme, className, ...remaining }: ButtonProps) => {
  const buttonStyle = ButtonStyles[scheme || "primary"];
  return (
    <button
      className={classNames(
        "py-2 text-white rounded-lg px-7",
        className,
        buttonStyle
      )}
      {...remaining}
    >
      {children}
    </button>
  );
};

export default Button;
