import React from "react";

type ButtonProps = React.ButtonHTMLAttributes<HTMLButtonElement>;

const LinkButton = ({ children, ...remaining }: ButtonProps) => {
  return (
    <button className="text-blue-500 hover:underline" {...remaining}>
      {children}
    </button>
  );
};

export default LinkButton;
