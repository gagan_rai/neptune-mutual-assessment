import React from "react";
import classNames from "classnames";

const NumericRegex = RegExp("^\\d*[.]?\\d*$");

type InputProps = Omit<React.HTMLProps<HTMLInputElement>, "onChange"> & {
  onChange?: (value: string) => void;
};

const InputNumber = ({ onChange, className, ...extraProps }: InputProps) => {
  function handleChange(value: string) {
    if (value === "" || NumericRegex.test(value)) onChange && onChange(value);
  }

  return (
    <input
      className={classNames(
        "w-full outline-none px-4 py-5 bg-slate-800 text-white rounded-lg",
        className
      )}
      inputMode="decimal"
      type="text"
      onChange={(e) => handleChange(e.target.value)}
      {...extraProps}
    />
  );
};

export default InputNumber;
