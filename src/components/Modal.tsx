import React from "react";
import { RiCloseLine } from "react-icons/ri";

type ModalProps = {
  visible: boolean;
  children: React.ReactNode;
  close: () => void;
  title?: string;
};

const Modal = ({ visible, children, close, title }: ModalProps) => {
  if (!visible) return null;
  return (
    <div className="fixed inset-0 z-10">
      <div className="absolute inset-0 bg-black opacity-50" />
      <div className="absolute inset-0 z-20 flex items-start justify-center">
        <div className="relative w-[520px] m-5 bg-slate-900 rounded-md mt-32">
          <div className="flex justify-between p-9">
            <h2 className="text-xl text-white">{title}</h2>
            <RiCloseLine
              onClick={close}
              className="text-2xl cursor-pointer text-slate-500 hover:text-white"
            />
          </div>
          <div className="px-9 pb-9">{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
