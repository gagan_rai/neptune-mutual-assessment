import NeptuneSVG from "@/components/NeptuneSVG";
import AccountButton from "@/modules/AccountButton";
import TokenConvertor from "@/modules/TokenConvertor";

export default function Home() {
  return (
    <main className="min-h-screen bg-primary">
      <header className="w-full border-b border-slate-50/[0.06]">
        <nav className="flex flex-col items-center justify-between px-5 py-5 mx-auto max-w-7xl md:flex-row">
          <div className="mb-3 md:mb-0">
            <NeptuneSVG />
          </div>
          <AccountButton />
        </nav>
      </header>
      <div className="px-5 pt-32 pb-20 flex-center">
        <TokenConvertor />
      </div>
    </main>
  );
}
