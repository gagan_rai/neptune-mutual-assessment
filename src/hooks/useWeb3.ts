import { useEffect, useState } from 'react'
import { useWeb3React } from "@web3-react/core";
import { Web3Provider } from "@ethersproject/providers";
import { Token } from '@uniswap/sdk'

import { defaultConnector } from "@/libs/Connectors";
import { formatAmount } from '@/libs/utils';

const useWeb3 = () => {
  const [balance, setBalance] = useState('0')
  const [token, setToken] = useState<Token>()
  const { account, activate, chainId, deactivate, library } = useWeb3React<Web3Provider>()

  useEffect(() => {
    if (account && chainId) {
      setToken(new Token(chainId, account, 18))
    }
  }, [account, chainId])

  useEffect(() => {
    if (account && token) {
      library?.getBalance(account).then((balance) => {
        setBalance(formatAmount(token, balance.toString()))
      })
    }
  }, [account, library, token])

  function login() {
    activate(defaultConnector)
  }

  const logout = deactivate

  return {
    account,
    chainId,
    balance,
    login,
    logout
  }
}

export default useWeb3
