"use client";
import { useState } from "react";
import { RiArrowUpDownLine } from "react-icons/ri";

import InputNumber from "../components/InputNumber";
import LinkButton from "@/components/LinkButton";
import WalletDetailsModal from "./WalletDetailsModal";

const Tokens = {
  nep: { displayName: "NEP", symbol: "nep", descriptiveName: "NEP" },
  busd: { displayName: "BUSD", symbol: "busd", descriptiveName: "Binance USD" },
};

const ConversionChart: Record<string, number> = {
  nep_busd: 3,
  busd_nep: 0.33333333333,
};

const TokenConvertor = () => {
  const sourceToken = Tokens["nep"];
  const targetToken = Tokens["busd"];
  const [sourceAmount, setSourceAmount] = useState("");
  const [targetAmount, setTargetAmount] = useState("");

  const [isWalletDetailsVisible, setWalletDetailsVisible] = useState(false);

  function calculateAndFormat(value: string, conversionSymbol: string) {
    const conversionRate = ConversionChart[conversionSymbol];
    if (!conversionRate || value === "") return "";
    const result = parseFloat(value) * conversionRate;
    if (!result) return "";
    return result.toFixed(2);
  }

  function handleSourceInput(value: string) {
    setSourceAmount(value);
    setTargetAmount(
      calculateAndFormat(value, `${sourceToken.symbol}_${targetToken.symbol}`)
    );
  }

  function handleTargetInput(value: string) {
    setTargetAmount(value);
    setSourceAmount(
      calculateAndFormat(value, `${targetToken.symbol}_${sourceToken.symbol}`)
    );
  }

  return (
    <div className="w-[520px]">
      <h1 className="text-4xl text-white">Convert Token</h1>
      <div className="mt-7">
        <div className="mb-5">
          <label
            htmlFor="source-amount-input"
            className="font-bold text-slate-300"
          >
            {sourceToken.displayName}
          </label>
          <InputNumber
            id="source-amount-input"
            title={sourceToken.descriptiveName}
            className="mt-3 text-2xl"
            value={sourceAmount}
            onChange={handleSourceInput}
            placeholder="0.00"
          />
        </div>
        <div className="flex justify-center">
          <RiArrowUpDownLine className="text-3xl text-slate-500" />
        </div>
        <div>
          <label
            htmlFor="target-amount-input"
            className="font-bold text-slate-300"
          >
            {targetToken.displayName}
          </label>
          <InputNumber
            id="target-amount-input"
            title={targetToken.descriptiveName}
            className="mt-3 text-2xl"
            value={targetAmount}
            onChange={handleTargetInput}
            placeholder="0.00"
          />
        </div>
      </div>
      <div className="flex justify-center mt-7">
        <LinkButton onClick={() => setWalletDetailsVisible(true)}>
          View Wallet Details
        </LinkButton>
      </div>
      <WalletDetailsModal
        visible={isWalletDetailsVisible}
        close={() => setWalletDetailsVisible(false)}
      />
    </div>
  );
};

export default TokenConvertor;
