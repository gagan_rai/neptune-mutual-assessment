import Modal from "@/components/Modal";
import Button from "@/components/Button";
import { formatAccount } from "@/libs/utils";
import useWeb3 from "@/hooks/useWeb3";

const WalletNotConnected = () => {
  return (
    <div className="mb-8">
      <p className="text-white">
        To view wallet details, you must connect your wallet first. Please click
        the &quot;Connect Wallet&quot; button to connect the wallet.
      </p>
    </div>
  );
};

type WalletDetailsProps = {
  account: string;
  chainId?: number;
  balance?: string;
};

const WalletDetails = ({ account, chainId, balance }: WalletDetailsProps) => {
  return (
    <div className="flex flex-col text-white mb-9">
      <div className="flex justify-between py-3">
        <h3 className="font-bold">Account</h3>
        <p>{formatAccount(account)}</p>
      </div>
      <div className="flex justify-between py-3">
        <h3 className="font-bold">Chain ID</h3>
        <p>{chainId}</p>
      </div>
      <div className="flex justify-between py-3">
        <h3 className="font-bold">Balance</h3>
        <p>{balance}</p>
      </div>
    </div>
  );
};

type WalletDetailsModalProps = {
  visible: boolean;
  close: () => void;
};

const WalletDetailsModal = ({ visible, close }: WalletDetailsModalProps) => {
  const { account, chainId, balance, login, logout } = useWeb3();

  return (
    <Modal title="Wallet Details" visible={visible} close={close}>
      {account ? (
        <WalletDetails account={account} chainId={chainId} balance={balance} />
      ) : (
        <WalletNotConnected />
      )}
      {account ? (
        <div className="flex justify-center">
          <Button scheme="danger" onClick={logout}>
            Disconnect Wallet
          </Button>
        </div>
      ) : (
        <div className="flex justify-end">
          <Button className="text-sm" onClick={login}>
            Connect Wallet
          </Button>
          <Button scheme="secondary" className="ml-2 text-sm" onClick={close}>
            Cancel
          </Button>
        </div>
      )}
    </Modal>
  );
};

export default WalletDetailsModal;
