"use client";
import Button from "@/components/Button";
import useWeb3 from "@/hooks/useWeb3";
import { formatAccount } from "@/libs/utils";

const AccountButton = () => {
  const { account, login } = useWeb3();

  if (!account) {
    return (
      <Button onClick={login} scheme="outline">
        Connect Wallet
      </Button>
    );
  }

  return <Button scheme="outline">{formatAccount(account)}</Button>;
};

export default AccountButton;
