"use client";
import React from "react";
import { Web3ReactProvider } from "@web3-react/core";
import { Web3Provider as Web3ProviderLib } from "@ethersproject/providers";

function getLibrary(provider: any) {
  const library = new Web3ProviderLib(provider);
  library.pollingInterval = 12000;
  return library;
}

const Web3Provider = ({ children }: { children: React.ReactNode }) => {
  return (
    <Web3ReactProvider getLibrary={getLibrary}>{children}</Web3ReactProvider>
  );
};

export default Web3Provider;
